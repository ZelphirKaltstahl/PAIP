(add-to-load-path (dirname (current-filename)))

#|
Lisp to Scheme translation:

defun -> define
setf -> define / let
defparameter ->
  global definition of a constant,
  probably define in Scheme,
  conventionally using *const-name* as name
case -> ?
function / #' (needed when referring to a function not in the first place of a list) -> ?
list -> (car (last lst))
mapcar -> map
funcall (like apply, but arguments are separate, not in a list) ->
  Just call the function.
  There is no namespace separation of variables and functions in Scheme.
  Thus funcall is not needed.
  When there is a separation, one needs to tell that a function is being called and not a variable accessed.
defvar --> define / let
|#
